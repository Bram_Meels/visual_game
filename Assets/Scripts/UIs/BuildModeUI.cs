﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildModeUI : MonoBehaviour {

    public Text ResourceValueDisplay;

    public GameObject TowerButtonPrefab;

    public Button[] TowerButtons;

    void Awake()
    {
        //Manual Button Value Instantiation
        for (int i = 0; i < TowerButtons.Length; i++)
        {
            TowerButtons[i].GetComponentInChildren<Text>().text = "Tower " + (i+1) + " (Cost: " + GameManager.Instance.BuildModeController.TowerCosts[i] + ")";

            int index = i;
            TowerButtons[i].onClick.AddListener(delegate { SwitchSelectedTower(index); });
        }

        //TODO: Programmatic Button Value Instantiation

        //for (int i = 0; i < GameManager.Instance.BuildModeController.TowerPrefabs.Length; i++)
        //{
        //    var towerButton = Instantiate(TowerButtonPrefab);
        //}

        RefreshTowerButtonDisplay();
        RefreshResourceValueDisplay();
    }

    public void RefreshResourceValueDisplay() {
        ResourceValueDisplay.text = "Resources :" + GameManager.Instance.Resources.ToString();
    }

    public void RefreshTowerButtonDisplay() {
        //TODO: change button graphics depending on events.
        for (int i = 0; i < TowerButtons.Length; i++)
        {
            ColorBlock buttonCB = TowerButtons[i].colors;
            if (i == GameManager.Instance.BuildModeController.SelectedTowerTypeIndex)
            {
                buttonCB.normalColor = new Color32(0xFD, 0xC8, 0x3C, 0xFF);
                //buttonCB.normalColor = Color.red;
                Debug.Log("Button selected");
            }
            else {
                buttonCB.normalColor = Color.white;
                Debug.Log("Button not selected");
            }
            TowerButtons[i].colors = buttonCB;

            TowerButtons[i].interactable = GameManager.Instance.BuildModeController.TowerUnlocked[i];
        }
    }

    void SwitchSelectedTower(int index) {
        GameManager.Instance.BuildModeController.SelectedTowerTypeIndex = index;
        Debug.Log("Selected Tower Index :" + GameManager.Instance.BuildModeController.SelectedTowerTypeIndex);
        RefreshTowerButtonDisplay();
    }
}
