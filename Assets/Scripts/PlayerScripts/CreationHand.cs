﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreationHand : MonoBehaviour {

    public GameObject PlayerHand;

    public enum SkillSelection
    {
        CreateTurret,
        SetTrap,
        Open
    }
    public SkillSelection skillSelection = SkillSelection.CreateTurret;

    public float AnimationLengthTime = 0;
    private float AnimationTime = 0;
    private Vector3 RestingPosition;
    private bool _playAnimation = false;

	// Use this for initialization
	void Start () {
        RestingPosition = PlayerHand.transform.localPosition;
	}
	
	// Update is called once per frame
	void Update () {
        SelectSkills();

        if (Input.GetMouseButtonDown(0))
        {
            switch (skillSelection)
            {
                case SkillSelection.CreateTurret:
                    Debug.Log("Create Turret");
                    CreateTurret();
                    break;
                case SkillSelection.SetTrap:
                    Debug.Log("Set trap down");
                    break;
                case SkillSelection.Open:
                    Debug.Log("Can open stuff now");
                    break;
            }
            _playAnimation = true;
        }
        HandAnimation();
	}

    private void HandAnimation()
    {
        if (_playAnimation)
        {
            if (AnimationTime < AnimationLengthTime)
            {
                AnimationTime += Time.deltaTime;
                PlayerHand.transform.Translate(Vector3.down * (Time.deltaTime) / 10);
            }
            if(AnimationTime > AnimationLengthTime)
            {
                AnimationTime = 0;
                _playAnimation = false;
                Debug.Log("Put hand back");
                PlayerHand.transform.localPosition = RestingPosition;
            }
        }
    }

    private void SelectSkills()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            skillSelection = SkillSelection.CreateTurret;
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            skillSelection = SkillSelection.SetTrap;
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            skillSelection = SkillSelection.Open;
        }
    }

    private void CreateTurret()
    {
        Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        RaycastHit raycastHit;
        if(Physics.Raycast(ray, out raycastHit))
        {
            if(raycastHit.collider.tag == "BuildBlock")
            {
                Debug.Log("I see the building block");
            }
        }
    }

    private void SetTrap()
    {

    }
}
