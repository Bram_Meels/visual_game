﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour {

    private bool _jumping = false;
    public float JumpTimerLimit = 0;
    private float JumpTimer = 0;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        KeyboardControl();
        Jumping();
	}

    private void KeyboardControl()
    {
        //Movement
        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(0, 0, 0.1f, Space.Self);
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(0, 0, -0.1f, Space.Self);
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(-0.1f, 0, 0, Space.Self);
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(0.1f, 0, 0, Space.Self);
        }

        //Jumping
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            StartJump();
        }

        //Basic input
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            //Open menu
        }
    }

    private void StartJump()
    {
        _jumping = true;
    }

    private void Jumping()
    {
        if (_jumping && JumpTimer < JumpTimerLimit)
        {
            JumpTimer += Time.deltaTime;
            transform.position += new Vector3(0, 1, 0) * (JumpTimer / JumpTimerLimit);
        }
        if(_jumping && JumpTimer > JumpTimerLimit)
        {
            _jumping = false;
            JumpTimer = 0;
        }
    }
}
