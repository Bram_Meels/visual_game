﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    private GameObject shooter;

    public GameObject Shooter
    {
        get
        {
            return shooter;
        }

        set
        {
            shooter = value;
        }
    }

    void OnTriggerExit(Collider collision) {
        //Bullet is destroyed when outside of tower range
        if (collision.name == "TowerRange" && collision.transform.parent.gameObject == shooter) {
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter(Collider collision) {
        if (collision.tag == "Enemy")
        {
            collision.gameObject.GetComponent<Enemy>().Health -= shooter.GetComponent<Tower>().BulletDamage;
            Destroy(gameObject);
        }
    }
    
}
