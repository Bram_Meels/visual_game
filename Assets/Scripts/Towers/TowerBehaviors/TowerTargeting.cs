﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerTargeting : MonoBehaviour {

    public GameObject TowerHead;

    private GameObject target;

    public GameObject GetTarget() {
        return target;
    }

    void Update() {
        if (target != null)
        {
            TowerHead.transform.LookAt(target.transform);
        }
    }

    void OnTriggerStay(Collider collision) {
        if (target == null)
        {
            if (collision.tag == "Enemy")
            {
                target = collision.gameObject;
            }
        }
    }
    void OnTriggerExit(Collider collision) {
        if (collision.tag == "Enemy")
        {
            target = null;
        }
    }
}
