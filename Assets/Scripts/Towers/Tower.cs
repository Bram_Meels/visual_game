﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour {

    public GameObject BulletPrefab;
    public GameObject BulletSpawnArea;
    public SphereCollider TowerRangeTrigger;

    public float BulletVelocity = 50;
    public float BulletDamage = 1;
    public float TowerRange = 10;
    public float TowerCooldown = 1;

    public float[] DamageIncreasePerLevel;
    public float[] RangeIncreasePerLevel;
    public float[] CooldownDecreasePerLevel;
    public int[] UpgradeCostPerLevel;

    private int currentTowerLevel;
    private int maxTowerLevel = 3;

    private float currentTowerCooldown;
    void Start() {
        currentTowerLevel = 1;
        currentTowerCooldown = 1;
        TowerRangeTrigger.radius = TowerRange;
        //TowerRangeTrigger.enabled = false;
    }

    void Update() {
        if (!GameManager.Instance.InBuildMode) {
            if (GetComponent<TowerTargeting>().GetTarget() != null) {
                Shoot();
            }
        }
    }

    void Shoot() {
        if (currentTowerCooldown >= TowerCooldown)
        {
            GameObject bullet = Instantiate(BulletPrefab, BulletSpawnArea.transform.position, BulletSpawnArea.transform.rotation);
            bullet.GetComponent<Rigidbody>().velocity = BulletSpawnArea.transform.forward * BulletVelocity;
            bullet.GetComponent<Bullet>().Shooter = gameObject;
            currentTowerCooldown = 0;
        }
        else {
            currentTowerCooldown += 1 * Time.deltaTime;
        }
    }

    void Upgrade() {
        if (currentTowerLevel < maxTowerLevel)
        {
            GameManager.Instance.Resources -= UpgradeCostPerLevel[currentTowerLevel - 1];
            currentTowerCooldown -= CooldownDecreasePerLevel[currentTowerLevel - 1];
            TowerRange += RangeIncreasePerLevel[currentTowerLevel - 1];
            BulletDamage += DamageIncreasePerLevel[currentTowerLevel - 1];
            currentTowerLevel++;
        }
        
    }


}
