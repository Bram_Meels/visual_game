﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    private static GameManager instance = null;
    public static GameManager Instance
    {
        get
        {
            return instance;
        }
    }

    public BuildModeController BuildModeController
    {
        get
        {
            return buildModeController;
        }

        set
        {
            buildModeController = value;
        }
    }
    private BuildModeController buildModeController;

    public bool InBuildMode;

    private int resources;
    public int Resources
    {
        get
        {
            return resources;
        }

        set
        {
            resources = value;
        }
    }

    // Use this for initialization
    void Awake() {
        if (instance == null)
        {
            instance = this;
        }
        else {
            Destroy(gameObject);
        }
        buildModeController = GetComponent<BuildModeController>();

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        InBuildMode = true;

        resources = 300;
    }

    void Start() {
        buildModeController.EnterBuildMode();
    }

	// Update is called once per frame
	void Update () {
        if (InBuildMode) {
            if (Input.GetKeyDown(KeyCode.Space)) {
                InBuildMode = false;
                buildModeController.ExitBuildMode();
            }
        }
	}
}
