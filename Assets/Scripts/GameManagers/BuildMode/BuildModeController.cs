﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildModeController : MonoBehaviour {

    public GameObject BuildModeUI;

    public GameObject[] TowerPrefabs;
    public bool[] TowerUnlocked;
    public int[] TowerCosts;

    private int selectedTowerTypeIndex = 0;
    public int SelectedTowerTypeIndex
    {
        get
        {
            return selectedTowerTypeIndex;
        }

        set
        {
            selectedTowerTypeIndex = value;
        }
    }

    private List<GameObject> towers = new List<GameObject>();

    private GameObject BuildAreas;

    void Awake() {
        BuildModeUI = Instantiate(BuildModeUI);
        BuildAreas = GameObject.Find("BuildAreas");
    }

    public void EnterBuildMode() {
        foreach (var tower in towers)
        {
            tower.GetComponent<Tower>().TowerRangeTrigger.enabled = false;
        }
        BuildAreas.gameObject.SetActive(true);
    }

    public void ExitBuildMode() {
        foreach (var tower in towers)
        {
            tower.GetComponent<Tower>().TowerRangeTrigger.enabled = true;
        }
        BuildAreas.gameObject.SetActive(false);
        BuildModeUI.GetComponent<Canvas>().enabled = false;
    }

    public GameObject GetSelectedTowerType() {
        return TowerPrefabs[selectedTowerTypeIndex];
    }

    public void AddTower(GameObject tower) {
        GameManager.Instance.Resources -= TowerCosts[selectedTowerTypeIndex];
        towers.Add(tower);
        BuildModeUI.GetComponent<BuildModeUI>().RefreshResourceValueDisplay();
    }
}
