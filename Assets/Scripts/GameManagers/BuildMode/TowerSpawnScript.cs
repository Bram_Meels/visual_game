﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerSpawnScript : MonoBehaviour {

    private bool occupied;

    void Start() {
        occupied = false;
    }

    void OnMouseOver() {
        if (Input.GetKeyDown(KeyCode.Mouse0) 
            && GameManager.Instance.InBuildMode )
        {
            if (GameManager.Instance.Resources >= GameManager.Instance.BuildModeController.TowerCosts[GameManager.Instance.BuildModeController.SelectedTowerTypeIndex])
            {
                GameObject tower = Instantiate(
                GameManager.Instance.BuildModeController.GetSelectedTowerType(),
                new Vector3(transform.position.x, 1, transform.position.z),
                transform.rotation
                );
                GameManager.Instance.BuildModeController.AddTower(tower);
                tower.GetComponent<Tower>().TowerRangeTrigger.enabled = false;

                occupied = true;
            }
            else {
                Debug.Log("Not Enough Resources!");
            }
        }
    }
}
