﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerLoader : MonoBehaviour {

    public GameObject GameManagerPrefab;

	void Awake () {
        if (GameManager.Instance == null) {
            GameManagerPrefab = Instantiate(GameManagerPrefab);
        }
	}
}
