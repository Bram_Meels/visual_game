﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour {

    private Camera _Camera;
    public Camera Camera2;
    public GameObject PlayerCharacter;
    public GameObject PlayerHands;

    private Vector3 MainCameraoffset;

    public float minVertical = -45f;
    public float maxVertical = 45f;

    public float sensitivityHorizontal = 10.0f;
    public float sensitivityVertical = 10.0f;

    public float _rotationX = 0;


	// Use this for initialization
	void Start () {
        _Camera = Camera.main;
        Camera.main.enabled = true;
        Camera2.enabled = false;
	}
	
	// Update hapens as Last
	void LateUpdate () {

        if (_Camera.enabled)
        {
            FirstPersonMode();
            //So the camera follows the player only in firstperson mode
            _Camera.transform.position = PlayerCharacter.transform.position;
        }

        //Switching the camera's from position : topview, firstperson
        if (Input.GetKeyDown(KeyCode.M))
        {
            if (_Camera.enabled)
            {
                Camera2.enabled = true;
                _Camera.enabled = false;
            }
            else
            {
                _Camera.enabled = true;
                Camera2.enabled = false;
            }
        }
    }

    private void FirstPersonMode()
    {
        {
            _Camera.transform.Rotate(0, Input.GetAxis("Mouse X") * sensitivityHorizontal, 0);
        
            _rotationX -= Input.GetAxis("Mouse Y") * sensitivityVertical;
            _rotationX = Mathf.Clamp(_rotationX, minVertical, maxVertical);//clamps the number between the minimal and maximum so(between -45 and 45)

            float rotationY = _Camera.transform.localEulerAngles.y;

            _Camera.transform.localEulerAngles = new Vector3(_rotationX, rotationY, 0);
            PlayerCharacter.transform.localEulerAngles = Vector3.up * rotationY;
            PlayerHands.transform.localEulerAngles = Vector3.right * _rotationX;
        }
    }
}
